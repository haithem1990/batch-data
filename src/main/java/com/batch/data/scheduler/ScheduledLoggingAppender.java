package com.batch.data.scheduler;

import com.batch.data.Holder;

import java.util.TimerTask;

public class ScheduledLoggingAppender extends TimerTask {

    @Override
    public void run() {
        Holder.getInstance().work();
    }
}
