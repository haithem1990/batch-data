package com.batch.data;

import com.batch.data.model.BackendBatchCall;
import com.batch.data.model.BackendCall;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class Holder {

    private static Holder holder;

    // thread safe concurrency queue
    private Queue<BackendBatchCall> queueBackendCall;

    private Long bachID;

    private Holder() {
        queueBackendCall = new ConcurrentLinkedQueue<>();
        bachID = 1L;
    }

    public static Holder getInstance() {
        if (holder == null) {
            holder = new Holder();
        }
        return holder;
    }

    public void add(BackendCall backendCall) {
        BackendBatchCall backendBatchCall = new BackendBatchCall(bachID, backendCall);
        queueBackendCall.offer(backendBatchCall);
    }

    // this method will be executed with a scheduler
    public void work() {
        // increment the bach id
        bachID += 1;
        List<BackendCall> backendCallList = new ArrayList<>();
        while (!queueBackendCall.isEmpty() && queueBackendCall.element().getBatchId() < bachID) {
            // polling data from the queue
            backendCallList.add(queueBackendCall.poll().getBackendCall());
        }

        // save polled data
        save(backendCallList);
    }

    private void save(List<BackendCall> backendCallList) {
        System.out.println("######################################");
        backendCallList.stream().forEach(backendCall -> System.out.println(backendCall.getCall()));
        System.out.println("######################################");
    }

}
