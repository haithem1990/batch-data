package com.batch.data.model;

public class BackendBatchCall{

    private Long batchId;

    private BackendCall backendCall;

    public BackendBatchCall(Long batchId, BackendCall backendCall) {
        this.batchId = batchId;
        this.backendCall = backendCall;
    }

    public Long getBatchId() {
        return batchId;
    }

    public void setBatchId(Long batchId) {
        this.batchId = batchId;
    }

    public BackendCall getBackendCall() {
        return backendCall;
    }

    public void setBackendCall(BackendCall backendCall) {
        this.backendCall = backendCall;
    }
}
