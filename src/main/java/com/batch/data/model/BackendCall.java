package com.batch.data.model;

public class BackendCall {

    private String call;

    public BackendCall(String call) {
        this.call = call;
    }

    public String getCall() {
        return call;
    }

    public void setCall(String call) {
        this.call = call;
    }
}
