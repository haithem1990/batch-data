package com.batch.data;

import com.batch.data.model.BackendCall;
import com.batch.data.scheduler.ScheduledLoggingAppender;

import java.util.Timer;

public class Main {
    public static void main(String[] args) {
        Timer timer = new Timer();
        timer.schedule(new ScheduledLoggingAppender(), 5000, 5000);
        Holder holder = Holder.getInstance();
        int i = 1;
        while (true) {
            holder.add(new BackendCall(String.valueOf(i)));
            i++;
        }
    }
}
